/*
 * Exercise 2: Prime Sieve
 *
 * Using what we now know about built-in javascript array methods, we're
 * going to build one of the coolest algorithms around, a prime sieve! Your
 * mission, should you choose to accept it, is to take the following array
 * of numbers (1-100 inclusive) and filter out all numbers that are not primes!
 * 
 * The programming isn't the hard part here, but you do want to think about a couple
 * of things:
 * 
 * 1.) Keeping track of found prime numbers (how are you going to store a found prime).
 * 
 * 2.) Using the modulo '%' operator is an AWESOME tool for this.
 * 
 * 3.) For those of you who really want to go hard-mode, there's a super nifty
 *     optimization to the algorithm, where for some number (n), we only need consider
 *     any primes in the range from i^2 to n, where i is some number in our range 1-n.
 * 
 * 4.)  To make this easier, I've provided a nice starting block.
 */

function primeSieve(arrayIn){
    var foundPrimes = [];

}

var array = []

for(i=1; i<=100; i++){
    array.push(i)
}

console.log("Here is our starting array: ",array);

primeSieve(array);