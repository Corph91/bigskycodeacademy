/*
 * ARRAY MAGIC:
 * 
 * Last time we worked with React, we got to see some of the power of using objects.
 * We've done this before, but just to freshen up (and like a pianist playing scales)
 * let's work out some cool stuff we can do.
 * 
 * Here you see the data for a class of twenty students. Your goal is to (using only 
 * the functions and data in the object and requisite array/object methods:
 * 
 * 1.) Populate the array of score data objects. Think of a method that
 *     returns an array for here, and assign it back to the appropriate
 *     place in the object! Also, don't forget many array methods
 *     have a second argument:
 *     
 *          array.map(function(item,index){
 *              ...
 *          });
 * 
 * 2.) Using the functions built in to our object, calculate the average
 *     score for the class and assign it to the object's average key.
 *     Note that I'm going to challenge you here to create your own
 *     averaging function!
 */

var data = {
    names: ["John","Linda","Mark","Marigold","Monique",
            "Montell","Ryan","Aaron","Sue","Pablo",
            "John","Jessica","Doug","Jenn","Samantha",
            "Katie","Tylor","Cody","Sienna","Quincy"
    ],
    score_data: [
        {},{},{},{},{},
        {},{},{},{},{},
        {},{},{},{},{},
        {},{},{},{},{},
    ],
    raw_scores: [66,60,89,89,75,
                92,45,52,99,90,
                88,82,85,73,76,
                81,91,51,63,97
                ],
   assignData: function(student,score,data){
       return data[student] = score;
   },
   calcAverage: function(){
       // YOUR CODE HERE. What should be passed in?
   },
   average: 0
}


//SOLUTION 1:


//SOLUTION 2: