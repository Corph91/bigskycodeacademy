/*
 * This is an algorithm called merge sort! Without delving too much into
 * the nerd side, it continually splits up and array, and then (once it
 * splits up the array into its individual elements) merges those
 * split segments back together two at a time (sorting each of the elements in
 * the two segments against each other).
 *
 * Don't worry about implementing the
 * algorithm for now. What I want you to do here is, thinking about
 * how we've brought in outside resources into our server.js file for our
 * express app, figure out how you can get the mergeSort function to "talk"
 * with the array in main.js, such that you can call the mergeSort function
 * on the array in main.js!
 */

var mergeSort = function(arr)
{
    if (arr.length < 2){
      return arr;
    }

    var middle = parseInt(arr.length / 2);
    var left   = arr.slice(0, middle);
    var right  = arr.slice(middle, arr.length);

    return merge(mergeSort(left), mergeSort(right));
}

function merge(left, right)
{
    var result = [];

    while (left.length && right.length) {
        if (left[0] <= right[0]) {
            result.push(left.shift());
        } else {
            result.push(right.shift());
        }
    }

    while (left.length)
        result.push(left.shift());

    while (right.length)
        result.push(right.shift());

    return result;
}
<<<<<<< HEAD

module.exports.mergeSort = mergeSort;
=======
>>>>>>> 9b382dda79ef68725823960f7d702762a71476af
