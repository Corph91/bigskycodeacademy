console.log("Hello ES6!");

function greeter(name){
    console.log("Hello "+name+"!")
}

greeter("Sean")

// ES6 version
const greeting = (name) => {
    return "Hello again, "+name+"!"
};

console.log(greeting("Sean"))

const greetMe = (name) => "Hello one more time, "+name+ "!";

console.log(greetMe("Sean"));

const greetMe2 = name => "Hawwo, "+name+"!";

console.log(greetMe2("Sean"));

//Template String
const greetMeTemp = name => `Hello there ${name}, how are you?`;

console.log(greetMeTemp("Sean"));

var numbers = [1,2,3,4,5,6];

const getSum = (a,b) => a + b;

const mappedNumbers = numbers.map((item) => { return item + 10});

console.log(mappedNumbers)

// Enhanced object literals

var first = "Sean";
var last = "Corbett";

const person = {first, last};

//V2

//old way:

var me = {
    name: "Sean",
    friends: ["Arizona","Collin","George"],
    sayHello: function(){
        var self = this;
        return this.friends.map(function(item){
            return item + " " + self.name + "'s friend' ";
        });
    }
}

console.log(me.sayHello())

const you = {
    name: "Sue",
    friends: ["JK","Lolz","Winzz"],
    sayHello(){
        return this.friends.map((item) => {
            return `${item} is ${this.name}'s friend.`
        });
    }
}

console.log(you.sayHello())